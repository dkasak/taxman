# taxman - Automates calculation of Croatian authorship taxes and filling
#          out the JOPPD form (up to date with 2017/2018 tax law)
#
# Copyright (c) 2017, 2018 Denis Kasak, Dinko Galetić

from textwrap import TextWrapper
import decimal
from decimal import Decimal as D
from datetime import datetime
from urllib.request import urlopen
from collections import namedtuple

ExchangeRate = namedtuple('ExchangeRate', ['date', 'rate', 'currency'])
Payment = namedtuple('Payment', ['amount', 'date', 'currency'])

decimal.getcontext().rounding = decimal.ROUND_HALF_UP

fmt       = "{:8}{:45} {}"
date_fmt  = "%Y-%m-%d"
reference = D('1.00')

default_currency      = 'USD'
stopa_osnovice_za_dop = D('0.7')
stopa_prvi_stup       = D('0.075')
stopa_drugi_stup      = D('0.025')
porezna_stopa         = D('0.24')


def print_wrapped(field_number, field_number_width, field_name, field_value):
    wrapper = TextWrapper(initial_indent='',
                          width=70,
                          subsequent_indent=' ' * field_number_width)

    lines = wrapper.wrap(field_name)

    print('{num:{width}}{name}'.format(num=field_number,
                                       name=lines[0],
                                       width=field_number_width))

    for line in lines[1:]:
        print(line)

    print('{space:{width}}{value}'.format(space='',
                                          width=field_number_width + 4,
                                          value=field_value))


# kvantizira rezultat na određeni broj decimalnih mjesta iza točke, sa
# zaokruživanjem
def q(d):
    global reference
    return d.quantize(reference)


def disclaimer():
    print("Pretpostavke: porez 24%, sva nespomenuta polja koja traže iznos u kn su 0.")


def hnb_mean_exchange_rate(currency, date):
    dt = datetime.strptime(date, '%Y-%m-%d')
    date = dt.strftime('%Y-%m-%d')

    if currency.upper() == 'HRK':
        return ExchangeRate(date, D(1), currency)

    hnb_file_name = 'f{}.dat'.format(dt.strftime('%d%m%y'))
    hnb_url = 'http://www.hnb.hr/tecajn/{}'.format(hnb_file_name)

    with urlopen(hnb_url) as data:
        # Skip the header line
        data.readline()
        hnb_data = [elem.strip().decode('utf-8').split()
                    for elem in data.readlines()]

    for elem in hnb_data:
        if currency.lower() == str(elem[0])[3:6].lower():
            # return date and its mean rate (from the third column)
            return ExchangeRate(date,
                                D(elem[2].replace(',', '.')),
                                currency)


def should_continue():
    answer = input("Još primitaka? [Y/n]: ")

    if not answer.strip() or answer.lower() == 'y' or answer.lower() == 'yes':
        return True
    else:
        return False


def read_currency():
    currency = input("Kôd valute primitka [USD]: ").strip()

    while not currency.isalpha() and len(currency) != 0:
        print("\t-> Pogrešan unos. Kôd valute sastoji se od tri slova.")
        currency = input("Kôd valute primitka [{}]: ".format(default_currency)).strip()

    if len(currency) == 0:
        return default_currency
    else:
        return currency.upper()


def read_date():
    while True:
        date = input("Datum primitka [danas]: ").strip()

        if not date:
            return datetime.now().strftime(date_fmt)

        try:
            datetime.strptime(date, date_fmt)
        except ValueError:
            print("\t-> Pogrešan unos. Datum treba unijeti u formatu YYYY-MM-DD.")
            continue

        return date


def read_transactions():
    primici = []

    while True:
        currency = read_currency()

        primitak = input("Iznos primitka u {}: ".format(currency)).strip()

        amount = q(D(primitak))
        date = read_date()

        primici.append(Payment(amount, date, currency))

        if not should_continue():
            break

    return primici


def print_tax_report(primici):
    prirez_stopa = q(D(input("Prirez u tvojem mjestu (decimalno, e.g. 0.14): ")))

    predujam_total = D('0')
    prvi_stup_total = D('0')
    drugi_stup_total = D('0')

    for i, primitak in enumerate(primici):
        ex_rate = hnb_mean_exchange_rate(primitak.currency, primitak.date)

        iznos = primitak.amount
        tecaj = ex_rate.rate

        total = q(tecaj * iznos)

        print()
        print('Redak {} ({}, {})'.format(i+1, primitak.date, primitak.currency))
        print('=========================')
        print(fmt.format("11.", "Iznos primitka:", total))
        osnovica_za_dop = q(stopa_osnovice_za_dop * total)
        print(fmt.format("12.", "Osnovica za obračun doprinosa:", osnovica_za_dop))
        prvi_stup = q(stopa_prvi_stup * osnovica_za_dop)
        print(fmt.format("12.1.", "Doprinos za mirovinsko:", prvi_stup))
        drugi_stup = q(stopa_drugi_stup * osnovica_za_dop)
        print(fmt.format("12.2.", "Doprinos za II. stup:", drugi_stup))
        izdatak = q(total - osnovica_za_dop)
        print(fmt.format("13.1.", "Izdatak:", izdatak))
        mirovinsko_ukupno = q(prvi_stup + drugi_stup)
        print(fmt.format("13.2.", "Izdatak — uplaćeni doprinos za mirovinsko:", mirovinsko_ukupno))
        dohodak = q(osnovica_za_dop - mirovinsko_ukupno)
        print(fmt.format("13.3.", "Dohodak:", dohodak))
        porezna_osnovica = dohodak
        print(fmt.format("13.5.", "Porezna osnovica:", porezna_osnovica))
        porez = q(porezna_osnovica * porezna_stopa)
        print(fmt.format("14.1.", "Iznos poreza:", porez))
        prirez = q(porez * prirez_stopa)
        print(fmt.format("14.2.", "Iznos prireza:", prirez))
        iznos_za_isplatu = q(total - mirovinsko_ukupno - porez - prirez)
        print(fmt.format("16.2.", "Iznos za isplatu:", iznos_za_isplatu))

        predujam_total += porez + prirez
        prvi_stup_total += prvi_stup
        drugi_stup_total += drugi_stup

    print()
    print("Stranica A: ")
    print_wrapped(
        "V.5.", 8,
        "Ukupan iznos predujma poreza na dohodak i prireza porezu na dohodak po "
        "osnovi primitka od kojeg se utvrđuje drugi dohodak:",
        predujam_total)
    print_wrapped(
        "VI.1.2.", 8,
        "Ukupan iznos doprinosa za mir. os. na temelju gen. solid. po osnovi drugog "
        "dohotka:",
        prvi_stup_total)
    print_wrapped(
        "VI.2.2.", 8,
        "Ukupan iznos doprinosa za mir. os. na temelju indiv. kap. štednje po "
        "osnovi drugog dohotka:",
        drugi_stup_total)


def main():
    disclaimer()
    primici = read_transactions()
    print_tax_report(primici)


if __name__ == '__main__':
    main()
